module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      height:{
        '40vh': "40vh"
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
