import sys
import os
import pandas as pd
from SPARQLWrapper import SPARQLWrapper

sparql = SPARQLWrapper("http://127.0.0.1:3030/FilmParis/update")

if __name__ == "__main__":
    directory = os.path.dirname(os.path.abspath(__file__))
    data_raw = pd.read_csv(f"{directory}/lieux-de-tournage-a-paris2.csv", sep=",")
    for row in data_raw.iterrows():
        index = row[0]
        datas = row[1]
        titre = datas["Titre"]
        real = datas["Réalisateur"]
        codePostal = datas["Code postal"]
        posLat = datas["Coordonnée en X"]
        posLon = datas["Coordonnée en Y"]
        
        #print(f"({titre},{real},{codePostal},{posLat},{posLon})")
        
        sparql.setQuery(f"""
            prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            prefix : <http://www.semanticweb.org/adminetu/ontologies/2021/2/untitled-ontology-3#>
            insert data
            {{ 
                :Film_{index} a :Film.
                :Realisateur_{index} a :Réalisateur;
                rdfs:label "{real}".
                :Film_{index} :a_comme_titre "{titre}".
                :Realisateur_{index} :a_realise :Film_{index}.
                :position_x_{index} a :position_x;
                rdfs:label "{posLat}".
                :position_y_{index} a :position_y;
                rdfs:label "{posLon}".
                :codepostal_{index} a :codepostal;
                rdfs:label "{codePostal}".
                :Film_{index} :a_comme_position_x :position_x_{index}.
                :Film_{index} :a_comme_position_y :position_y_{index}.
                :Film_{index} :se_deroule_a :codepostal_{index}.
            }}
            """)
        sparql.method
        try:
            res = sparql.query().convert() # res is a stream with the results in XML, see <http://www.w3.org/TR/rdf-sparql-˓→XMLres/>
            print(res)
        except:
            e = sys.exc_info()
            print(e)


