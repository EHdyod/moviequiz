import sys
import os
import pandas as pd
from SPARQLWrapper import SPARQLWrapper, JSON
import json  

sparql = SPARQLWrapper("https://dbpedia.org/sparql")

if __name__ == "__main__":
    directory = os.path.dirname(os.path.abspath(__file__))
    data_raw = pd.read_csv(f"{directory}/lieux-de-tournage-a-paris2.csv", sep=",")
    films = []
    i = 0
    for row in data_raw.iterrows():
        index = row[0]
        datas = row[1]
        titre = datas["Titre"]
        real = datas["Réalisateur"]
        codePostal = datas["Code postal"]
        posLat = datas["Coordonnée en X"]
        posLon = datas["Coordonnée en Y"]
        
        
        sparql.setQuery(f"""
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX dbpedia: <http://dbpedia.org/resource>
            PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>
            PREFIX dbpprop: <http://dbpedia.org/property/>
            
            SELECT ?titre (?rname as ?real) ?date ?resume
            WHERE
            {{ 
                ?film a <http://dbpedia.org/ontology/Film>.
	            ?film dbpedia-owl:abstract ?resume.
                ?film rdfs:label ?titre .
                ?film dbpedia-owl:director ?real .
                ?real rdfs:label ?rname .
  	            FILTER regex(?titre, "{titre}", "i")
                FILTER regex(?rname, "{real}", "i")  
                FILTER langMatches(lang(?titre), 'en')
                FILTER langMatches(lang(?resume), 'en')
                FILTER langMatches(lang(?rname), 'en')
            }}
            LIMIT 1
            """)
        sparql.method
        sparql.setReturnFormat(JSON)
        resume=""
        try:
            results = sparql.query().convert()
            nbfilms = len(results["results"]["bindings"])
            if nbfilms == 1 :
                result = results["results"]["bindings"][0]
                resume = result['resume']['value']
                
                if(resume != ""):
                    films.append({"titre": titre, "realisateur":real, "resume": resume, "codePostal": datas["Code postal"], "posLat": datas["Coordonnée en X"], "posLon": datas["Coordonnée en Y"]})
                    i+=1
        except:
            e = sys.exc_info()
            print(e)
    print(f"find {i} films.")
    with open("films.json", "w") as outfile: 
        json.dump(films, outfile)
